#!/bin/bash

_update()
{
	sudo -u $USER_NAME makepkg --verifysource

	cd kicad
	git fetch
	cd ..
}

_kicadRawVersion()
{
	cd kicad
	git describe --tag
	cd ..
}

_kicadVersion()
{
	_kicadRawVersion | sed 's/-/_/g'
}

_kicadHash()
{
	cd kicad
	git rev-parse --short master
	cd ..
}

_buildPKGBUILD()
{
	kicadVersion=`_kicadVersion`

	sed -e "s/@KICAD_VERSION@/$kicadVersion/" \
		-e "s/@KICAD_HASH@/$(_kicadHash)/" \
		PKGBUILD.in > PKGBUILD
}

_updateSRCINFO()
{
	rm .SRCINFO
	sudo -u $USER_NAME makepkg --printsrcinfo > .SRCINFO
}

# check if root (makepkg must not be run as root)
if [ "$EUID" -ne 0 ]; then
	USER_NAME=`whoami`
else
	if [ -z "$USER_NAME" ]; then
		echo "USER_NAME not set"
		exit
	fi
fi

echo "Bumping kicad-nightly"

cd kicad-nightly

_update
_buildPKGBUILD
_updateSRCINFO

commitMsg="Bump to KiCad $(_kicadRawVersion)"

git add PKGBUILD .SRCINFO
git commit -m "$commitMsg"
git push origin master

export PKG_VER=`_kicadVersion`

cd ..

echo $commitMsg
