# _fetchRepo(repo_url:$1, repo_name:$2)
_fetchRepo() {
	echo "Fetching $2..."
	# Remove existing repo
	if [ -d $2 ]; then rm -rf $2; fi
	# Clone repo
	git clone $1 $2
	# Filtering not recognized by the AUR, leaving here for future reference
	# git clone --filter=tree:0 --single-branch --branch master $1 $2
	chown -hR $USER_NAME:$USER_NAME $2
}

# check if root (other scripts will need non elevated access)
if [ "$EUID" -ne 0 ]; then
	USER_NAME=`whoami`
else
	if [ -z "$USER_NAME" ]; then
		echo "USER_NAME not set"
		exit
	fi
fi

_fetchRepo ssh://aur@aur.archlinux.org/kicad-nightly.git kicad-nightly
_fetchRepo ssh://aur@aur.archlinux.org/kicad-library-nightly.git kicad-library-nightly
