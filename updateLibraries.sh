#!/bin/bash

_update()
{
	sudo -u $USER_NAME makepkg --verifysource

	cd kicad-symbols
	git fetch
	cd ../kicad-footprints
	git fetch
	cd ../kicad-packages3D
	git fetch
	cd ..
}

_symbolRawVersion()
{
	cd kicad-symbols
	git describe --tag
	cd ..
}

_symbolHash()
{
	cd kicad-symbols
	git rev-parse --short master
	cd ..
}

_footprintRawVersion()
{
	cd kicad-footprints
	git describe --tag
	cd ..
}

_footprintHash()
{
	cd kicad-footprints
	git rev-parse --short master
	cd ..
}

_packages3dRawVersion()
{
	cd kicad-packages3D
	git describe --tag
	cd ..
}

_package3dHash()
{
	cd kicad-packages3D
	git rev-parse --short master
	cd ..
}

_buildPKGBUILD()
{
	sed -e "s/@KICAD_VERSION@/$PKG_VER/" \
	-e "s/@SYMBOL_HASH@/$(_symbolHash)/" \
	-e "s/@FOOTPRINT_HASH@/$(_footprintHash)/" \
	-e "s/@PACKAGE3D_HASH@/$(_package3dHash)/" \
	PKGBUILD.in > PKGBUILD
}

_updateSRCINFO()
{
	rm .SRCINFO
	sudo -u $USER_NAME makepkg --printsrcinfo > .SRCINFO
}

# check if root (makepkg must not be run as root)
if [ "$EUID" -ne 0 ]; then
	USER_NAME=`whoami`
else
	if [ -z "$USER_NAME" ]; then
		echo "USER_NAME not set"
		exit
	fi
fi

# check that version is set
if [ -z "$PKG_VER" ]; then
	echo "PKG_VER not set (provided by updatePackage.sh)"
	exit
fi

echo "Bumping kicad-library-nightly"

cd kicad-library-nightly

_update
_buildPKGBUILD
_updateSRCINFO

commitMsg="Bump to KiCad-Symbols $(_symbolRawVersion), KiCad-Footprints $(_footprintRawVersion) and KiCad-Packages3D $(_packages3dRawVersion)"

lastCommit=$(git log -n 1 --pretty=format:"%s")

if [ "$lastCommit" != "$commitMsg" ]; then
	git add PKGBUILD .SRCINFO
	git commit -m "$commitMsg"
	git push origin master
fi

cd ..

echo $commitMsg
