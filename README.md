KiCad Arch Builder
=================

If you are looking to run KiCad or KiCad nightly on Arch Linux, please head over to https://www.kicad.org/download/arch-linux/.

This repository holds the tools to setup and provide the moving nightly package.

Binary Repo
===========

The repository automatically bumps and builds the latest nightly packages and makes them available in static page/binary repo.

The repository is available at:
https://kicad.gitlab.io/packaging/kicad-arch/kicad-arch-builder/

You can have your system fetch and install these builds automatically by configuring pacman (the package manager) and adding a new repository.

To do this you can append this entry to the end of the configuration file:

```
> /etc/pacman.conf

# Nightly builds for KiCad
[kicad-nightly]
SigLevel = Optional TrustAll
Server = https://kicad.gitlab.io/packaging/kicad-arch/kicad-arch-builder/
```

You should now be able to install the latest nightly with:
```
pacman -Syu
pacman -S kicad-nightly
```


**WARNING**: The builds in this repository are currently not being signed. **Use at your own risk**

**CAUTION**: Currently library packages are not prebuilt due to some constraints and must be installed through the aur manually, you can simplify the installation through the use of an aur helper.

Setup
=====

`This refers to the automatic build setup in the repository`

gitlab CI/CD pipelines are used to automaticaly bump and build the packages on a set schedule (Usually midnight EST time), and consits of 4 stages, bump (checks out latest version of kicad repos and builds the new PKGBUILDs), compile (compiles the new packages), build (builds a database that is used by pacman to determine what packages are being provided) and finally deploy (this builds a static web page binary repo a deploys to github pages)

The necessary steps to have a working pipeline are:
- provide a SSH private key secret variable in repo config, for pushes to aur
- setup a CI/CD Schedule to trigger the pipeline
- set the job timeout, builds are long and may take up to 3 hours to complete

Usage
=====

`This refers to the manual 'bumping' of the nightly packages`

3 scripts are provided that allow this: `fetchRepos.sh`, `updatePackage.sh` and `updateLibraries.sh`

Some steps are required before running these utilities:
- make sure you are checked out to the latest commit of this repository
`git pull`
- make sure all submodules are cloned and checked out to the latest commit (**WARNING** this step discards any changes inside the submodules)
`./fetchRepos.sh`

Run the scripts in the root of this repository
`source ./updatePackage.sh`
`source ./updateLibraries.sh`

Some notes:
- `updatePackage.sh` must be ran before `updateLibraries.sh` as this provides the `PKG_VER` variable required by it
- If you run the scripts as root you need to provide a NON root user, this is done through the variable `USER_NAME`
    `ex.: USER_NAME=dummy source ./updatePackage.sh`
